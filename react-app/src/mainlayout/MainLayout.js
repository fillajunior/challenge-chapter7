import React from "react-router-dom";

import Index from "../pages/Index";


const MainLayout = () => {
  return (
    <div>
      <Index />
    </div>
  )
}

export default MainLayout
