import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

// https://raw.githubusercontent.com/fnurhidayat/probable-garbanzo/main/data/cars.min.json

ReactDOM.render(<App/>, document.getElementById('root'));