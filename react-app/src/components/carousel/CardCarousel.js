import React from "react";

const CardCarousel = () => {
  return (
    <div class="item p-4 " style={{
        backgroundColor:"#F1F3FF"
    }}>
      <div class="row justify-content-center align-items-center ">
        <div className="col-3">
          <img
            src="img/photocewe.png"
            alt=""
            style={{ width: "80px" }}
            className="mx-auto"
          />
        </div>
        <div className="col-9" 
        >
            
          <i
            className="bi bi-star-fill"
            style={{
              color: "#f9cc00",
              
          
            }}
          ></i>
          <i
            className="bi bi-star-fill"
            style={{
              color: "#f9cc00",
            }}
          ></i>
          <i
            className="bi bi-star-fill"
            style={{
              color: "#f9cc00",
            }}
          ></i>
          <i
            className="bi bi-star-fill"
            style={{
              color: "#f9cc00",
            }}
          ></i>
          <i
            className="bi bi-star-fill"
            style={{
              color: "#f9cc00",
            }}
          ></i>
          <div className="comment">
            "Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorem,
            aliquid obcaecati quibusdam repellendus suscipit doloremque beatae
            iure odit. Totam libero odit facilis iste consectetur numquam!"
          </div>
          <div
            className="profile mt-2"
            style={{
              fontSize: "18px",
              fontWeight: "bolder",
            }}
          >
            Ash 20, Singapore
          </div>
        </div>
      </div>
    </div>
  );
};

export default CardCarousel;
