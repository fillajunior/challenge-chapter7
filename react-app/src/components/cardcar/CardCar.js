import React from "react";

import "bootstrap/dist/css/bootstrap.min.css";

function  CardCar() {
  return (

<main>
    {/* Card */}
    <section className="container">
      <div className="card card-pesan mx-auto" onclick="activeDarkBackground()">
        <div className="card-body">
          <div className="row">
            <div className="col">
              <label htmlFor>Tipe Driver</label>
              <select className="form-select" id="driver" required>
                <option value>Pilih Tipe Driver</option>
                <option className="option-item" value={1}>Dengan Driver</option>
                <option className="option-item" value={2}>Tanpa Driver</option>
              </select>
            </div>
            <div className="col">
              <label htmlFor>Tanggal</label>
              <input type="date" className="form-control" id="date" required />
            </div>
            <div className="col">
              <label htmlFor>Pilih Waktu</label>
              <select className="form-select" id="time">
                <option value>Pilih Waktu</option>
              </select>
            </div>
            <div className="col">
              <label htmlFor>Jumlah Penumpang</label>
              <input type="number" id="passenger" className="form-control" placeholder="Jumlah Penumpang" />
            </div>
            <div className="col" onclick="inactiveDarkBackground()">
              <button className="btn btn-success text-white fw-bold mt-3" onclick="btnSearch()">Cari
                Mobil</button>
            </div>
          </div>
        </div>
      </div>
    </section>
    {/* End Card */}
    {/* <button id="load-btn">Load</button> */}
    {/* <button id="clear-btn">Clear</button> */}
    {/* Galeri */}
    <div className="container mt-3">
      <div className="row row-cols-lg-3 row-cols-sm-2 row-cols-1 g-3 justify-content-center " id="cars-container">
        {/* Card Data From JSON */}
      </div>
    </div>
  </main>

);

}

export default CardCar;