import React from "react";
import { Link } from "react-router-dom";

import "bootstrap/dist/css/bootstrap.min.css";

function Header() {
  return (
<header className="blue-1">
  {/* Navbar */}
  <nav className="navbar navbar-light navbar-expand-md fixed-top blue-1">
    <div className="container">
      <a href="#"><img className="logo-bawah" src="img\logo.png" alt="" /></a>
      <button className="navbar-toggler " type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasNavbar" aria-controls="offcanvasNavbar">
        <span className="navbar-toggler-icon" />
      </button>
      <div className="offcanvas offcanvas-end" tabIndex={-1} id="offcanvasNavbar" aria-labelledby="offcanvasNavbarLabel" style={{maxWidth: '50%'}}>
        <div className="offcanvas-header">
          <h5 className="offcanvas-title" id="offcanvasNavbarLabel">BCR</h5>
          <button type="button" className="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close" />
        </div>
        <div className="offcanvas-body">
          <ul className="navbar-nav justify-content-end flex-grow-1 pe-3">
            <li className="nav-item">
              <a className="nav-link mx-2 text-black" href="#our-service">Our Service</a>
            </li>
            <li className="nav-item">
              <a className="nav-link mx-2 text-black" href="#why-us">Why Us</a>
            </li>
            <li className="nav-item">
              <a className="nav-link mx-2 text-black" href="#testimonial">Testimonial</a>
            </li>
            <li className="nav-item">
              <a className="nav-link mx-2 text-black" href="#faq">FAQ</a>
            </li>
            <li className="nav-item">
              <a className="btn btn-success mx-2 text-black fw-bold text-white" href="#">Register</a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </nav>
  {/* End Navbar */}
  {/* Home */}
  <div className="container-fluid">
    <div className="row home-content pt-5 mt-5">
      <div className="col-lg my-auto" style={{paddingLeft: '7%'}}>
        <h1>Sewa &amp; Rental Mobil Terbaik di kawasan Yogyakarta</h1>
        <p>Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas terbaik dengan harga
          terjangkau. Selalu siap melayani kebutuhanmu untuk sewa mobil selama 24 jam.</p>
          <Link to={`/Cars`}>
          <a className="btn btn-success text-white fw-bold" id="mulaiSewa">Mulai Sewa Mobil</a>
        </Link>

       
      </div>
      <div className="col-lg mt-4">
        <img className="img-fluid" src="img\car.png" alt="" style={{float: 'right'}} />
      </div>
    </div>
  </div>
  {/* End Home */}
</header>




  );

}

export default Header;
