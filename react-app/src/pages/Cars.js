import React from 'react'
import HeaderCar from '../components/header/HeaderCar'
import FooterCar from '../components/footer/FooterCar'
import Filter from '../components/filter/Filter'
// import CardCar from '../components/cardcar/CardCar'

const Cars = () => {
    return(
        <div>
        <HeaderCar/>
       <Filter/>

       <FooterCar/>

        </div>
    )
}
export default Cars